package org.openapitools.interceptors;

import org.openapitools.exceptions.HmacSignatureInvalidException;
import org.openapitools.security.HmacReader;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.util.ContentCachingRequestWrapper;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Logger;

public class RequestInterceptor implements HandlerInterceptor {

    HmacReader hmacReader = new HmacReader();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpServletRequest wrapperRequest = new CustomHttpServletRequestWrapper(request);

        String hmacSignature = wrapperRequest.getHeader("x-hmac-signature");

        System.out.println(response.getStatus());

        if (hmacSignature != null) {
            System.out.println("Header x-hmac-signature found:");
            System.out.println(hmacSignature);
        }

        byte[] requestBodyBytes = readBodyBytes(wrapperRequest.getInputStream());

        System.out.println("Request body:");
        System.out.println(Arrays.toString(requestBodyBytes));

        String hexString = hmacReader.hmac(requestBodyBytes, "123456".getBytes(), "hmacsha256");

        System.out.println("HMAC parsed from body:");
        System.out.println(hexString);

        if(hexString.equals(hmacSignature)) {
            System.out.println("HMAC is valid");
        } else {
            System.out.println("HMAC is invalid");

            throw new HmacSignatureInvalidException();
        }

        return true;
    }

    private static byte[] readBodyBytes(ServletInputStream inputStream) throws IOException {
        byte[] buffer = new byte[1024];
        int bytesRead;
        StringBuilder bodyBuilder = new StringBuilder();

        while ((bytesRead = inputStream.read(buffer)) != -1) {
            bodyBuilder.append(new String(buffer, 0, bytesRead));
        }

        return bodyBuilder.toString().getBytes();
    }
}

