package org.openapitools;

import org.openapitools.exceptions.HmacSignatureInvalidException;
import org.openapitools.exceptions.UnauthorizedException;
import org.openapitools.exceptions.UserAlreadyExists;
import org.openapitools.exceptions.UserNotFound;
import org.openapitools.model.Error;
import org.openapitools.model.ResponseHeader;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.OffsetDateTime;
import java.util.UUID;

@ControllerAdvice
public class ExceptionMapper {

    @ExceptionHandler(HmacSignatureInvalidException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Error handleHmacSignatureInvalid() {
        return new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("HMAC_SIGNATURE_INVALID")
                .message("HMAC signature is invalid");
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Error handleBadRequest() {
        return new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("Bad request")
                .message("Arguments don't match");
    }

    @ExceptionHandler(UserAlreadyExists.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public Error handleUserAlreadyExists() {
        return new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("USER_ALREADY_EXISTS")
                .message("User already exists");
    }

    @ExceptionHandler(UserNotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public Error handleUserNotFound() {
        return new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("USER_NOT_FOUND")
                .message("User not found");
    }

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public Error handleUnauthorized() {
        return new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("UNAUTHORIZED")
                .message("Unauthorized");
    }
}
