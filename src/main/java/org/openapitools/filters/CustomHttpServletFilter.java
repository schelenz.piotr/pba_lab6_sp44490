package org.openapitools.filters;

import org.openapitools.interceptors.CustomHttpServletRequestWrapper;
import org.openapitools.interceptors.RequestInterceptor;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
@Order(1)
public class CustomHttpServletFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        CustomHttpServletRequestWrapper wrapperRequest = new CustomHttpServletRequestWrapper(httpServletRequest);
        filterChain.doFilter(wrapperRequest, servletResponse);
    }
}
