package org.openapitools.security;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class HmacReader {
    private static final String ENCODING = "UTF-8";

    public String hmac(byte[] text, byte[] key, String type)
            throws UnsupportedEncodingException, NoSuchAlgorithmException,
            InvalidKeyException {
        String alg = type;
        if (type == null || type == "") {
            alg = "hmacsha256";
        }
        SecretKeySpec secretKey = new SecretKeySpec(key, alg);
        Mac hmac = Mac.getInstance(alg);
        hmac.init(secretKey);

        byte[] digest = hmac.doFinal(text);

        return toHexString(digest);
    }

    /**
     * Gets the hmacsha256 hex string of given text.
     * @param text plain text string
     * @param key key string
     * @return hmacsha256 hex string
     */
    public String hmac(String text, String key) {
        String ret = text;
        try {
            ret = hmac(text.getBytes(ENCODING), key.getBytes(ENCODING),
                    "hmacsha256");
        } catch (InvalidKeyException e) {
            ret = text;
        } catch (UnsupportedEncodingException e) {
            ret = text;
        } catch (NoSuchAlgorithmException e) {
            ret = text;
        }
        return ret;
    }

    /**
     * Converts byte array to hex string
     *
     * @param bytes the byte array
     * @return the converted hex string
     */
    private String toHexString(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();
        for (byte mDigest : bytes) {
            String h = Integer.toHexString(0xFF & mDigest);
            while (h.length() < 2) {
                h = "0" + h;
            }
            hexString.append(h);
        }
        return hexString.toString();
    }
}
